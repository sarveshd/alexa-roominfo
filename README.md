Room Information Alexa Skill
============================

Supported conversation
----------------------
```
User:  "Alexa, open RoomInfo."
Alexa: "Welcome to our beautiful space. On your left are goldfish, to your right is the soda machine, and in front of you in me."
User:  "What can I do here?"
Alexa: "You can feed the fish, buy sodas, or talk with me."
User:  "What else can you do?"
Alexa: "I can tell you about the fish or sodas."
User:  "Tell me about the fish.'
Alexa: "There are three fish in the tank."
User:  "Tell me about the sodas."
Alexa: "There are delicious sodas to your right."
  ...  User pauses ...
Alexa: "Hello. What would you like to learn about?"
User:  "Tell me about the [anything other than fish or sodas]"
Alexa: "I'm sorry, I don't know anything about [noun above]. What else would you like to learn about?"
  ...  User pauses ...
Alexa: "Hello. What would you like to learn about?"
  ... User doesn't respond ...
Alexa: "Goodbye"
```
