from flask import Flask, request
import alexa

#################
# Configuration #
#################

APPLICATION_ID = 'amzn1.echo-sdk-ams.app.be4eb4fd-55ee-4c8e-b43e-f67806f795a3'

class Messages(object):
    WELCOME      = 'Welcome to our beautiful space. On your left are goldfish, to your right is the soda machine, and in front of you in me.'
    REPROMPT     = 'Hello. What would you like to learn about?'
    USER_ACTIONS = 'You can feed the fish, buy sodas, or talk with me.'
    ACTIONS      = 'I can tell you about the fish or sodas.'
    GOODBYE      = 'Goodbye'

    ABOUT = {
            'fish': 'There are three fish in the tank.',
            'soda': 'There are delicious sodas to your right.',
            'unknown':'''I'm sorry, I don't know anything about that. What else would you like to learn about?''',
            'unknown_thing':'''I'm sorry, I don't know anything about {Thing}. What else would you like to learn about?''',
            }
    ABOUT['fishes'] = ABOUT['fish']
    ABOUT['sodas']  = ABOUT['soda']

##########################

app = Flask(__name__)
app.debug=True

##########################
# Setup request handlers #
##########################

def launch_request():
    return alexa.AlexaResponse().output_speech(Messages.WELCOME).to_json()
    
def session_ended_request():
    return alexa.AlexaResponse().output_speech(Messages.GOODBYE).to_json()
    
alexa.setup_skill(app, route='/', app_id=APPLICATION_ID, launch_request=launch_request, session_ended_request=session_ended_request)

###########
# Intents #
###########

@alexa.intent(name='ListActions')
def list_actions_intent():
    return alexa.AlexaResponse().\
            output_speech(Messages.ACTIONS).\
            reprompt(Messages.REPROMPT).\
            to_json()

@alexa.intent(name='ListUserActions')
def list_user_actions_intent():
    return alexa.AlexaResponse().\
            output_speech(Messages.USER_ACTIONS).\
            reprompt(Messages.REPROMPT).\
            to_json()

@alexa.intent(name='DescribeThing')
def describe_thing_intent(thing):
    if thing is not None:
        if thing.lower() in Messages.ABOUT:
            message = Messages.ABOUT[thing.lower()]
        else:
            message = Messages.ABOUT['unknown_thing']
            message = message.replace('{Thing}', thing)
    else:
        message = Messages.ABOUT['unknown']

    return alexa.AlexaResponse().\
            output_speech(message).\
            reprompt(Messages.REPROMPT).\
            to_json()

##############################
# Non-Alexa request handling #
##############################

@app.route('/')
def fallback():
    return 'Room Information Skill for Amazon Echo'

@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, Nothing at this URL.', 404


@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500

##############################

if __name__ == '__main__':
    app.run()

