from flask import Flask, request, make_response
import json

#####

class AlexaException(Exception):
    pass

class InvalidContentTypeException(AlexaException):
    pass

class AlexaResponse(object):
    def __init__(self):
        self.version = '1.0'
        self.response = {}

        # mandatory
        self.should_end_session(False)

    def _speech(self, content):
        if type(content) in [ str, unicode ]:
            return {
                'type': 'PlainText',
                'text': content
            }

        raise InvalidContentTypeException()

    def output_speech(self, speech):
        self.response['outputSpeech'] = self._speech(speech)
        return self

    def reprompt(self, speech):
        self.response['reprompt'] = {}
        self.response['reprompt']['outputSpeech'] = self._speech(speech)
        return self

    def card(self, *args):
        raise NotImplementedError()

    def should_end_session(self, should_end_session):
        self.response['shouldEndSession'] = should_end_session
        return self

    def to_json(self):
        response = {
            'version':  self.version,
            'response': self.response,
        }

        return json.dumps(response)
        
#####

class AlexaSkill(object):
    def __init__(self, app, route, app_id=None, launch_request=None, session_ended_request=None):
        self.app = app
        self.app_id = app_id
        self.INTENT_HANDLERS = {}
        self.REQUEST_TYPE_HANDLERS = {
            'IntentRequest': self.intent_request,
            'LaunchRequest': launch_request,
            'SessionEndedRequest': session_ended_request,
        }
        
        self.app.add_url_rule(route, 'alexa_handler', self.handle_request, methods=['POST'])

    def handle_request(self):
        request_body = request.json

        if self.app_id is not None:
            if request_body['session']['application']['applicationId'] != self.app_id:
                return 'Bad application', 401

        request_type = request_body['request']['type']

        if request_type in self.REQUEST_TYPE_HANDLERS:
            response = make_response(self.REQUEST_TYPE_HANDLERS[request_type](), 200)
            response.headers['Content-Type'] = 'application/json'

            return response
        else:
            return 'Invalid request type "' + request_type + '"', 402

    def intent_request(self):
        return self.INTENT_HANDLERS[request.json['request']['intent']['name']]()
    
    def intent(self, name):
        """Decorator over intent handlers. Slots are passed as keyword args."""

        def intent_decorator(func):
            def wrapper():
                slot_dict = {}

                try:
                    slots = request.json['request']['intent']['slots']
                except KeyError as e:
                    # the 'slots' key is not present in the request
                    slots = []

                for slot in slots:
                    value = None
                    if 'value' in slots[slot]:
                        value = slots[slot]['value']

                    slot_dict[slot.lower()] = value

                return func(**slot_dict)

            self.INTENT_HANDLERS[name] = wrapper

            return wrapper

        return intent_decorator

# singleton for easy usage
_alexa_default_skill = None
_alexa_default_app = None

def _default_skill():
    global _alexa_default_skill

    if _alexa_default_app is None:
        raise AlexaException("Please call alexa.setup_skill() to set things up")

    if _alexa_default_skill is None:
        _alexa_default_skill = AlexaSkill(_alexa_default_app, '/')

    return _alexa_default_skill

def setup_skill(app, route, app_id, launch_request=None, session_ended_request=None):
    """Setup the default skill"""

    global _alexa_default_app

    if _alexa_default_app is None:
        _alexa_default_app = app

    if _alexa_default_app != app:
        raise AlexaException("Trying to setup multiple Alexa skill apps")

    skill = _default_skill()
    skill.app_id = app_id
    skill.REQUEST_TYPE_HANDLERS['LaunchRequest'] = launch_request
    skill.REQUEST_TYPE_HANDLERS['SessionEndedRequest'] = session_ended_request

#####

def intent(name):
    skill = _default_skill()
    return skill.intent(name)

#####


